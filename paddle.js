// Paddle 'object' 
function paddle(x, y, h, w)
{
	// set initial values
	this.x = x;
	this.y = y;
	this.h = h;
	this.w = w;
	
	// direction paddle is moving
	this.up = false;
	this.down = false;
	
	// points the paddle has scored
	this.points = 0;
	
	this.move = pMove;
}

// function to move the paddle
function pMove(yMod)
{
	var top = this.y + yMod;
	var bottom = top + this.h;
	
	if (yMod == Math.abs(yMod)) // moving up
	{
		if (top < 0)
		{
			this.y = 0;
		}
		else
		{
			this.y = top;
		}
	}
	else
	{
		if (bottom > canvas.height) // height of canvas
		{
			this.y = canvas.height - this.h
		}
		else
		{
			this.y = top;
		}
	}
	
	// Bounderies
	if (this.y < 0)
	{
		this.y = 0;
	}
	
	if ((this.y + this.h) > canvas.height)
	{
		this.y = canvas.height - this.h;
	}
}