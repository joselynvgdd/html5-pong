// ball 'object'
function ball(x, y, r, vx, vy)
{
	// set initial values
	this.x = x;
	this.y = y;
	this.r = r;
	this.vx = vx;
	this.vy = vy;
	
	this.move = bMove;
}

function bMove(elapsed)
{
	// how much the ball moves
	var xMod = this.vx * elapsed;
	var yMod = this.vy * elapsed;
	
	if (xMod == 0)
	{
		xMod = 1;
	}
	
	if (yMod == 0)
	{
		yMod = 1;
	}
	
	// apply motion to the ball
	this.x += xMod;
	this.y += yMod;

	// Collides with Floor/Ceiling
	if (this.y - this.r <= 0 || this.y + this.r >= canvas.height)
	{
		if (playsound)
		{
			hitSnd.play();
		}
		this.vy *= -1;
		if (this.y - this.r < 0)
		{
			this.y = this.r + 1;
		}
		else
		{
			this.y = canvas.height - this.r - 1;
		}
	}
	
	// Collides with player 1
	if (this.x - this.r <= player1.x+player1.w && ((this.y + this.r) >= player1.y && ((this.y - this.r) <= player1.y + player1.h)) )
	{
		if (playsound)
		{
			hitSnd.play();
		}
		
		if (this.vx > -1)
		{
			this.vx -= 0.01;
		}
		// revers the ball's x velocity
		this.vx *= -1;
		this.x = player1.x + player1.w + this.r + 1; // set the ball to the edge of the paddle
		
		// modify the ball's y velocity
		var bymod = this.y - player1.y;
		if (bymod >= 0 && bymod <= 50)
		{
			bymod = bymod/25 - 1;
			
			this.vy = this.vy + (0.25*bymod);
		}
	}
	
	if (this.x + this.r >= player2.x && ((this.y + this.r) >= player2.y && ((this.y - this.r) <= player2.y + player2.h)) )
	{
		if (playsound)
		{
			hitSnd.play();
		}
		
		if (this.vx < 1)
		{
			this.vx += 0.01;
		}
		// revers the ball's x velocity
		this.vx *= -1;
		this.x = player2.x - this.r - 1; // set the ball to the edge of the paddle
		
		// modify the ball's y velocity
		var bymod = this.y - player2.y;
		if (bymod >= 0 && bymod <= 50)
		{
			bymod = bymod/25 - 1;
			
			this.vy = this.vy + (0.25*bymod);
		}
	}
	
	// Points and Reset
	if (this.x - this.r <= 0 || this.x + this.r >= canvas.width)
	{
		this.vx *= -1;
		
		if (this.x - this.r < 0)
		{
			// Point P2
			player2.points += 1;
		}
		else
		{
			// Point P1
			player1.points += 1;
		}
		
		// set the ball to the center
		this.x = canvas.width/2;
		this.y = canvas.height/2;
		
		// get a new random velocity for the ball
		by = 0.0;
		bx = 0.0;
		while (by == 0.0 || by < -0.15 || by > 0.15)
		{
			by = (Math.random()*9)/10 + 0.1 - 0.5;
		}
		
		while (bx == 0.0 || bx < -0.15 || bx > 0.15)
		{
			bx = (Math.floor(Math.random()*3) - 1)  * 0.1;
		}
		
		this.vx = bx;
		this.vy = by;
	}
}