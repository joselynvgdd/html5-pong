function initCanvas()
{
	canvas = document.getElementById('canvas'); // get reference to the canvas
	
	if (canvas.getContext)
	{
		ctx = canvas.getContext('2d');
		initMenu();	// start the menu
	}
	
	playsound = false; // default sfx are off
	hitSnd = new Audio("boneImpact.wav");
}

function initMenu()
{
	// listen for menu keypresses
	window.addEventListener('keyup',menuInput,true);
	
	// set the canvas interval to run the menu logic at 60fps
	interval = setInterval(menuLogic, 1000/60);
}

function initPong()
{
	// listen for pong keypresses
	window.addEventListener('keydown',pongInput,true);
	window.addEventListener('keyup',stopInput,true);

	// create instances of paddles for each player
	player1 = new paddle(10, canvas.height/2-25, 50, 10);
	player2 = new paddle(canvas.width-20,canvas.height/2-25,50,10);
	
	// get a random velocity for the ball
	by = 0.0;
	bx = 0.0;
	while (by == 0.0 || by < -0.15 || by > 0.15)
	{
		by = (Math.random()*9)/10 + 0.1 - 0.5;
	}
	
	while (bx == 0.0 || bx < -0.15 || bx > 0.15)
	{
		bx = (Math.floor(Math.random()*3) - 1)  * 0.1;
	}
	
	// add the ball to the world
	theBall = new ball(canvas.width/2, canvas.height/2, 5, bx, by);
	
	// set the canvas interval to run the pong logic
	interval = setInterval(pongLogic, 1000/60);
}

function menuLogic()
{
	// menu only draws to the screen
	menuDraw();
}

function pongLogic()
{
	// get the elapsed time since last update
	var now = Date.now();
	var elapsed = (now - this.lastUpdate);
	this.lastUpdate = now;
	
	if (isNaN(elapsed))
	{
		elapsed = 0;
	}
	
	// update and draw the game
	pongUpdate(elapsed);
	pongDraw();
}

function pongUpdate(elapsed)
{
	var vel = 0.5; // vertical speed of the paddles
	// move the paddles
	if (player1.up)
	{
		player1.move((-vel)*elapsed);
	}
	
	if (player1.down)
	{
		player1.move((vel)*elapsed);
	}
	
	if (player2.up)
	{
		player2.move((-vel)*elapsed);
	}
	
	if (player2.down)
	{
		player2.move((vel)*elapsed);
	}
	
	// update the ball
	theBall.move(elapsed);
	
	// endgame conditions
	if (player1.points >= 5 || player2.points >= 5)
	{
		// stop listening for pong keypresses
		window.removeEventListener('keydown',pongInput,true);
		window.removeEventListener('keyup',stopInput,true);
		clearInterval(interval);	// clear the interval
		initMenu();	// go back to the menu
	}
}

function menuDraw()
{
	// Clear Screen
	ctx.fillStyle = "rgb(0,0,0)";
	ctx.fillRect(0,0,canvas.width,canvas.height);

	ctx.fillStyle = "rgb(255,255,255)";
	
	// Draw Menu
	ctx.font = "16pt Arial";
	ctx.fillText("HTML5 Pong", 10, 20);
	ctx.font = "12pt Arial";
	ctx.fillText("by Joselyn O'Connor", 10, 40);
	
	ctx.font = "10pt Arial";
	ctx.fillText("Press 'n' for a new game", 10, 80);
	
	if (playsound)
	{
		ctx.fillText("Press 'm' to turn off sfx", 10, 95);
	}
	else
	{
		ctx.fillText("Press 'm' to turn on sfx", 10, 95);
	}
	
	ctx.font = "12pt Arial";
	ctx.fillText("Controls", 10, 130);
	ctx.font = "10pt Arial";
	ctx.fillText("Left Player: W, S", 10, 145);
	ctx.fillText("Right Player: Up, Down", 10, 160);
}

function pongDraw()
{
	// Clear Screen
	ctx.fillStyle = "rgb(0,0,0)";
	ctx.fillRect(0,0,canvas.width,canvas.height);

	ctx.fillStyle = "rgb(255,255,255)";
	// Draw Scores
	ctx.font = "16pt Arial";
	ctx.fillText("Score: " + player1.points, 10, 20);
	ctx.fillText("Score: " + player2.points, canvas.width - 100, 20);
	
	// Draw Players
	ctx.fillRect(player1.x,player1.y,player1.w,player1.h);
	ctx.fillRect(player2.x,player2.y,player2.w,player2.h);
	
	// Draw Ball
	ctx.beginPath();
	ctx.arc(theBall.x, theBall.y, theBall.r, 0, Math.PI*2, true);
	ctx.fill();
	ctx.closePath();	
}

function menuInput(event)
{
	switch (event.keyCode)
	{
		case 77: /* M */
			// toggle playing sfx
			playsound = !playsound;
			break;
		case 78: /* N */
			//stop listening to menu keypresses
			window.removeEventListener('keyup',menuInput,true);
			clearInterval(interval); 
			initPong();	// start the pong games
			break;
	}
}

function pongInput(event)
{
	switch (event.keyCode) 
	{
		case 38:  /* Up arrow was pressed */
			player2.up = true;
			player2.down = false;
			break;
		case 40:  /* Down arrow was pressed */
			player2.up = false;
			player2.down = true;
			break;
		case 87: /* W was pressed */
			player1.up = true;
			player1.down = false;
			break;
		case 83: /* S was pressed */
			player1.up = false;
			player1.down = true;
			break;
	}
}

function stopInput(event)
{
	switch (event.keyCode) 
	{
		case 38:  /* Up arrow was released */
			player2.up = false;
			break;
		case 40:  /* Down arrow was released */
			player2.down = false;
			break;
		case 87: /* W was released */
			player1.up = false;
			break;
		case 83: /* S was released */
			player1.down = false;
			break;
	}
}
